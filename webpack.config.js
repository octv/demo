var Encore = require('@symfony/webpack-encore');

Encore
    // the project directory where compiled assets will be stored
    .setOutputPath('public/build/')
    // the public path used by the web server to access the previous directory
    .setPublicPath('/build')
    .cleanupOutputBeforeBuild()
    .enableSourceMaps(!Encore.isProduction())
    // .enableVersioning(Encore.isProduction())

    .enableSassLoader()

    .addEntry('octave/css/core', [
        './vendor/octv/octave-bundle/Bundle/CoreBundle/Resources/assets/scss/core.scss',
    ])
    .addEntry('octave/css/security', [
        './vendor/octv/octave-bundle/Bundle/SecurityBundle/Resources/assets/scss/security.scss'
    ])    

    .addEntry('octave/js/core', [
        './vendor/octv/octave-bundle/Bundle/CoreBundle/Resources/assets/js/core.js'
    ])

    // .createSharedEntry('vendor', [  ])

    // .enableVueLoader()

    .enableSingleRuntimeChunk()
;

module.exports = Encore.getWebpackConfig();